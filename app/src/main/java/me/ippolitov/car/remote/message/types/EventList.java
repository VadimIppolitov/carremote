package me.ippolitov.car.remote.message.types;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.EventType;
import me.ippolitov.car.remote.message.Initiator;
import me.ippolitov.car.remote.message.MsgType;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@Getter
public class EventList extends CarMessage {
    byte eventCount;
    int firstSeq; // unsigned 16-bit
    private List<Event> events;

    private EventList() {
        super(MsgType.MSG_EVENT_LIST);
    }

    public static EventList parse(ByteBuffer buffer) {
        EventList result = new EventList();
        result.eventCount = buffer.get();
        result.firstSeq = getUnsignedShort(buffer);
        result.events = new ArrayList<>(result.eventCount);
        for (int i = 0; i < result.eventCount; ++i) {
            result.events.add(Event.parse(buffer));
        }
        return result;
    }

    @Getter
    @ToString
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Event {
        private int secondsSinceEpoch;
        private EventType event;
        private Initiator initiator;
        private boolean success;

        public static Event parse(ByteBuffer buffer) {
            Event result = new Event();
            result.secondsSinceEpoch = buffer.getInt();
            result.event = EventType.byCode(buffer.get());
            result.initiator = Initiator.byCode(buffer.get());
            result.success = getBoolean(buffer);
            return result;
        }
    }
}
