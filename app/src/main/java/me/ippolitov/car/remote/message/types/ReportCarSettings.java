package me.ippolitov.car.remote.message.types;

import lombok.Getter;
import me.ippolitov.car.remote.CarSettings;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.MsgType;

import java.nio.ByteBuffer;

@Getter
public class ReportCarSettings extends CarMessage {
    private CarSettings settings;

    private ReportCarSettings() {
        super(MsgType.MSG_REPORT_SETTINGS);
    }

    public static ReportCarSettings parse(ByteBuffer buffer) {
        CarSettings settings = new CarSettings();
        settings.setVersion(getUnsignedShort(buffer));
        settings.setDriftCompensation(buffer.getShort());
        settings.setEngineTimeoutMillis(getUnsignedShort(buffer));
        settings.setAutostartEnabled(getBoolean(buffer));
        settings.setAutostartIntervalMinutes(getUnsignedShort(buffer));
        settings.setAutostartDurationMinutes(getUnsignedShort(buffer));

        ReportCarSettings result = new ReportCarSettings();
        result.settings = settings;
        return result;
    }
}
