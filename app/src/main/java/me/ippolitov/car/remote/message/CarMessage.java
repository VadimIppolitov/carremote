package me.ippolitov.car.remote.message;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import me.ippolitov.car.lib.Buffers;
import me.ippolitov.car.lib.Packet;
import me.ippolitov.car.remote.Crypto;
import me.ippolitov.car.remote.message.types.*;

import java.nio.ByteBuffer;
import java.util.Arrays;

@Getter
public abstract class CarMessage {
    public static int DRIFT_HACK = 76870;
    private final MsgType type;

    @Setter(AccessLevel.PROTECTED)
    private MsgSource source;
    @Setter(AccessLevel.PROTECTED)
    private int secondsSinceEpoch = 0;

    protected CarMessage(MsgType type) {
        this.type = type;
    }

    public Status asStatus() {
        return (Status) this;
    }

    public ReportCarSettings asReportCarSettings() {
        return (ReportCarSettings) this;
    }

    public Text asText() {
        return (Text) this;
    }

    public EventList asEventList() {
        return (EventList) this;
    }

    public static abstract class Sendable extends CarMessage {
        protected Sendable(MsgType type) {
            super(type);
        }

        protected abstract void doPack(ByteBuffer buffer);

        public ByteBuffer pack() {
            ByteBuffer buffer = Buffers.allocateBuffer();
            buffer.put(getType().getCode());
            doPack(buffer);
            buffer.flip();
            Nonce nonce = Nonce.create(getSecondsSinceEpoch(), MsgSource.REMOTE);
            return Crypto.encrypt(new Message(buffer, nonce));
        }
    }

    public static int getUnsignedShort(ByteBuffer buffer) {
        return buffer.getShort() & 0xffff;
    }

    public static void putUnsignedShort(ByteBuffer buffer, int value) {
        buffer.putShort((short) (value & 0xffff));
    }

    protected static boolean getBoolean(ByteBuffer buffer) {
        return buffer.get() != 0;
    }

    protected static void putBoolean(ByteBuffer buffer, boolean b) {
        buffer.put((byte) (b ? 1 : 0));
    }

    public static CarMessage unpack(Packet packet) {
        Message message = Crypto.decrypt(packet.byteBuffer());
        if (message == null) {
            return null;
        }
        if (!validIncomingMessageSource(message.getNonce().getSource())) {
            return null;
        }
        ByteBuffer buffer = message.getData();
        MsgType type = MsgType.byCode(buffer.get());
        CarMessage result;
        switch (type) {
            case MSG_STATUS:
                result = Status.parse(buffer);
                break;
            case MSG_REPORT_SETTINGS:
                result = ReportCarSettings.parse(buffer);
                break;
            case MSG_TEXT:
                result = Text.parse(buffer);
                break;
            case MSG_EVENT_LIST:
                result = EventList.parse(buffer);
                break;
            default:
                return null;
        }
        result.setSecondsSinceEpoch(message.getNonce().getSecondsSinceEpoch() + DRIFT_HACK);
        result.setSource(message.getNonce().getSource());
        return result;
    }

    private static boolean validIncomingMessageSource(MsgSource source) {
        return Arrays.asList(MsgSource.CAR_CONTROLLER).contains(source);
    }
}
