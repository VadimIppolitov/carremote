package me.ippolitov.car.remote.message.types;

import lombok.Getter;
import me.ippolitov.car.remote.message.Action;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.MsgSource;
import me.ippolitov.car.remote.message.MsgType;

import java.nio.ByteBuffer;
import java.util.Calendar;

@Getter
public class RequestAction extends CarMessage.Sendable {
    private Action action;

    public RequestAction(Action action, Calendar sendTime) {
        this();
        this.action = action;
        this.setSecondsSinceEpoch((int) (sendTime.getTimeInMillis() / 1000));
        this.setSource(MsgSource.REMOTE);
    }

    private RequestAction() {
        super(MsgType.MSG_REQUEST_ACTION);
    }

    @Override
    protected void doPack(ByteBuffer buffer) {
        buffer.put(action.getCode());
    }
}
