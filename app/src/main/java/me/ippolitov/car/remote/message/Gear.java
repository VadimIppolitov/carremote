package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum Gear {
    UNKNOWN('?'),
    PARKED('P'),
    ;

    private final char code;

    Gear(char code) {
        this.code = code;
    }

    public static Gear byCode(char code) {
        for (Gear gear : values()) {
            if (gear.code == code) {
                return gear;
            }
        }
        return UNKNOWN;
    }
}
