package me.ippolitov.car.remote.message.types;

import lombok.Getter;
import me.ippolitov.car.remote.CarSettings;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.MsgSource;
import me.ippolitov.car.remote.message.MsgType;

import java.nio.ByteBuffer;
import java.util.Calendar;

@Getter
public class ApplyCarSettings extends CarMessage.Sendable {
    private CarSettings settings;

    public ApplyCarSettings(CarSettings settings, Calendar sendTime) {
        this();
        this.settings = settings;
        this.setSecondsSinceEpoch((int) (sendTime.getTimeInMillis() / 1000));
        this.setSource(MsgSource.REMOTE);
    }

    private ApplyCarSettings() {
        super(MsgType.MSG_APPLY_SETTINGS);
    }

    @Override
    protected void doPack(ByteBuffer buffer) {
        putUnsignedShort(buffer, settings.getVersion());
        buffer.putShort((short) settings.getDriftCompensation());
        putUnsignedShort(buffer, settings.getEngineTimeoutMillis());
        putBoolean(buffer, settings.isAutostartEnabled());
        putUnsignedShort(buffer, settings.getAutostartIntervalMinutes());
        putUnsignedShort(buffer, settings.getAutostartDurationMinutes());
    }
}
