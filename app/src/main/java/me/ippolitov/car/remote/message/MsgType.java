package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum MsgType {
    UNKNOWN(0),
    MSG_STATUS(1),
    MSG_REQUEST_ACTION(2),
    MSG_TEXT(3),
    MSG_EVENT_LIST(6),
    MSG_REQUEST_EVENTS(7),
    MSG_APPLY_SETTINGS(8),
    MSG_REPORT_SETTINGS(9),
    ;

    private final byte code;

    MsgType(int code) {
        this.code = (byte) code;
    }

    public static MsgType byCode(byte code) {
        for (MsgType msgType : values()) {
            if (msgType.code == code) {
                return msgType;
            }
        }
        return UNKNOWN;
    }
}
