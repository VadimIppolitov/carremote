package me.ippolitov.car.remote;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import me.ippolitov.car.lib.Packet;

public class AlarmReceiver extends WakefulBroadcastReceiver {
    public static final String KEEP_ALIVE_INTENT = "me.ippolitov.car.remote.KEEP_ALIVE";
    public static final String CONNECTION_STATUS_INTENT = "me.ippolitov.car.remote.CONNECTION_STATUS";

    @Override
    public void onReceive(Context context, Intent intent) {
        CarRemoteApplication applicationContext = (CarRemoteApplication) context.getApplicationContext();
        sendKeepAlive(applicationContext);
        reportConnectionStatusToActivity(applicationContext);
    }

    private void sendKeepAlive(CarRemoteApplication applicationContext) {
        Packet packet = Packet.builder().putMagic().putKeepaliveMarker().build();
        applicationContext.getDataMediator().sendViaUDP(packet);
    }

    private void reportConnectionStatusToActivity(CarRemoteApplication applicationContext) {
        Intent connectionIntent = new Intent(CONNECTION_STATUS_INTENT);
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(connectionIntent);
    }
}
