package me.ippolitov.car.remote;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.ippolitov.car.lib.DataMediator;
import me.ippolitov.car.lib.DataMediatorSink;
import me.ippolitov.car.lib.Packet;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.types.EventList;

import java.util.Calendar;

@Slf4j
class IncomingMessageProcessor implements DataMediatorSink {

    private static final long SECONDS_DIFF_ALLOWED = 25;

    private CarRemoteApplication carRemoteApplication;

    @Getter
    private Calendar lastMessageTime = null;

    public IncomingMessageProcessor(CarRemoteApplication carRemoteApplication) {
        this.carRemoteApplication = carRemoteApplication;
    }

    @Override
    public void setDataMediator(DataMediator dataMediator) {
    }

    @Override
    public boolean handlePacket(final Packet packet) {
        final CarMessage message = CarMessage.unpack(packet);
        if (message == null) {
            log.trace("Bad message format?");
            return false;
        }

        Calendar now = carRemoteApplication.now();

        //  TODO: also check for real replay
        final int messageDriftSec = (int) (now.getTimeInMillis() / 1000) - message.getSecondsSinceEpoch();

        reportConnectionStatusToActivity(messageDriftSec);

        if (Math.abs(messageDriftSec) > SECONDS_DIFF_ALLOWED) {
//            runInLooper(new Runnable() {
//                public void run() {
//                    carRemoteApplication.showShortToast(String.format("Huge drift (%ds)", messageDriftSec));
//                }
//            });
            log.warn("Huge drift: {}", messageDriftSec);
            return true;
        }

        boolean messageProcessed = processMessage(message);

        lastMessageTime = now;

        return messageProcessed;
    }

    private boolean processMessage(final CarMessage message) {
        switch (message.getType()) {
            case MSG_TEXT:
                runInLooper(new Runnable() {
                    public void run() {
                        carRemoteApplication.showShortToast(message.asText().getText());
                    }
                });
                return true;
            case MSG_EVENT_LIST:
                runInLooper(new Runnable() {
                    public void run() {
                        EventList eventList = message.asEventList();
                        for (EventList.Event event : eventList.getEvents()) {
                            log.debug("Received event: {}", event);
                            //  TODO: show as actual list in interface
                            carRemoteApplication.showNotification(event);
                        }
                    }
                });
                return true;
            case MSG_REPORT_SETTINGS:
                CarSettings newSettings = message.asReportCarSettings().getSettings();
                if (newSettings.getVersion() > carRemoteApplication.getSettings().getVersion()) {
                    carRemoteApplication.setSettings(newSettings);
                }
                return true;
            case MSG_STATUS:
                carRemoteApplication.setCarStatus(message.asStatus());
                return true;
        }
        return false;
    }


    private void reportConnectionStatusToActivity(int lastMessageDriftSec) {
        Intent connectionIntent = new Intent(AlarmReceiver.CONNECTION_STATUS_INTENT);
        connectionIntent.putExtra("driftSec", lastMessageDriftSec);
        LocalBroadcastManager.getInstance(carRemoteApplication).sendBroadcast(connectionIntent);
    }

    private void runInLooper(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }
}
