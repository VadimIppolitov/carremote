package me.ippolitov.car.remote.message.types;

import lombok.Getter;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.MsgType;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

@Getter
public class Text extends CarMessage.Sendable {
    private String text;

    private Text() {
        super(MsgType.MSG_TEXT);
    }

    public static Text parse(ByteBuffer buffer) {
        Text result = new Text();
        byte[] buf = new byte[buffer.remaining()];
        buffer.get(buf);
        result.text = new String(buf).trim();
        return result;
    }

    @Override
    protected void doPack(ByteBuffer buffer) {
        buffer.put(text.getBytes(StandardCharsets.UTF_8));
    }
}
