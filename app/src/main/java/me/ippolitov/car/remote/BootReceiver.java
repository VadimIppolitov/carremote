package me.ippolitov.car.remote;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BootReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        log.info("Boot event received");
        //  Nothing to do here: all work is done in CarRemoteApplication
    }
}