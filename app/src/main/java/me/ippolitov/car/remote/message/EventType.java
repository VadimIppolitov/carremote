package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum EventType {
    UNKNOWN(0),
    ENGINE_START(31),
    ENGINE_STOP(32),
    DOOR_LOCK(34),
    DOOR_UNLOCK(35),
    ACC_ON(36),
    ACC_OFF(37),
    ;

    private final byte code;

    EventType(int code) {
        this.code = (byte) code;
    }

    public static EventType byCode(byte code) {
        for (EventType eventType : values()) {
            if (eventType.code == code) {
                return eventType;
            }
        }
        return UNKNOWN;
    }
}
