package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum MsgSource {
    UNKNOWN(0),
    PLAINTEXT(1),
    SERVER(11),
    CAR_CONTROLLER(21),
    CAR_PILOT(31),
    REMOTE(41),
    ;

    private final byte code;

    MsgSource(int code) {
        this.code = (byte) code;
    }

    public static MsgSource byCode(int code) {
        for (MsgSource source : values()) {
            if (source.code == code) {
                return source;
            }
        }
        return UNKNOWN;
    }
}
