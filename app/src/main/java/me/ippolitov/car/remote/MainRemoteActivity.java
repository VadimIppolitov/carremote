package me.ippolitov.car.remote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import lombok.extern.slf4j.Slf4j;
import me.ippolitov.car.lib.Utils;
import me.ippolitov.car.remote.message.Action;
import me.ippolitov.car.remote.message.Gear;
import me.ippolitov.car.remote.message.types.Status;

import java.util.Calendar;
import java.util.Locale;

@Slf4j
public class MainRemoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_remote);
        IntentFilter filter = new IntentFilter(AlarmReceiver.CONNECTION_STATUS_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(connectionStatusReceiver, filter);

        Switch autostartToggle = findViewById(R.id.autostartToggle);
        autostartToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                findViewById(R.id.autostartSettingsContainer).setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    public void applySettings(View view) {
        CarSettings settings = getCarRemoteApplication().getSettings();
        Switch autostartToggle = findViewById(R.id.autostartToggle);
        settings.setAutostartEnabled(autostartToggle.isChecked());
        int minutes = getAndFixAutostartMinutes();
        settings.setAutostartIntervalMinutes(minutes);
        getCarRemoteApplication().applySettings();
    }

    private int getAndFixAutostartMinutes() {
        TextView autostartEveryText = findViewById(R.id.autostartEveryText);
        String autostartOriginalText = autostartEveryText.getText().toString();
        int minutes;
        try {
            minutes = Integer.parseInt(autostartOriginalText);
        } catch (NumberFormatException e) {
            log.warn("Bad 'every minutes' string: {}", autostartOriginalText);
            minutes = 0;
        }
        minutes = Math.max(60, Math.abs(minutes));
        String autostartNewText = String.valueOf(minutes);
        if (!autostartOriginalText.equals(autostartNewText)) {
            autostartEveryText.setText(autostartNewText);
        }
        return minutes;
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(connectionStatusReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showCurrentStatus(null);
        requestAction(Action.ACTION_STATUS);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public void requestStatus(MenuItem menuItem) {
        requestAction(Action.ACTION_STATUS);
    }

    private void runInLooper(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    private void showGenerator(boolean isGenerator) {
        ImageView generatorStatus = findViewById(R.id.generatorStatus);
        if (isGenerator) {
            generatorStatus.setImageResource(R.drawable.ic_engine);
        } else {
            generatorStatus.setImageDrawable(null);
        }
    }

    private void showGear(Gear gear) {
        ImageView gearStatus = findViewById(R.id.gearStatus);
        if (gear == Gear.PARKED) {
            gearStatus.setImageResource(R.drawable.ic_park);
        } else {
            gearStatus.setImageDrawable(null);
        }
    }

    private void showStatus(Status status) {
        showGenerator(status.isGenerator());
        showGear(status.getGear());
        ((ToggleButton) findViewById(R.id.startEngine)).setChecked(status.isIgnition());
        ((ToggleButton) findViewById(R.id.accToggle)).setChecked(status.isAcc());
        String statusText;
        switch (status.getLastStatus()) {
            case UNKNOWN:
                statusText = "";
                break;
            case STARTED:
            case VERIFYING:
            case ENGINE_ERROR:
                float startTimeSec = status.getLastStarterTime() / 1000.f;
                statusText = String.format(Locale.US, "%s (%.1fs)", status.getLastStatus().name(), startTimeSec);
                break;
            default:
                statusText = status.getLastStatus().name();
                break;
        }
        if (getCarRemoteApplication().getSettings().isAutostartEnabled() && !status.isGenerator()) {
            statusText += String.format(Locale.US, " (scheduled start in %s)", Utils.formatDuration(status.getMinutesUntilScheduledStart() * 60));
        }
        TextView viewById = findViewById(R.id.statusText);
        viewById.setText(statusText.trim());
        String tempStr = String.format(Locale.US, "In car: %d°C", status.getTemperature());
        ((TextView) findViewById(R.id.temperatureView)).setText(tempStr);
    }

    public void startEngine(View view) {
        ToggleButton startEngine = findViewById(R.id.startEngine);
        Status carStatus = getCarRemoteApplication().getCarStatus();
        if (carStatus == null) {
            startEngine.setChecked(false);
        } else {
            startEngine.setChecked(carStatus.isIgnition());
        }
        requestAction(Action.ACTION_START);
    }

    public void stopEngine(View view) {
        requestAction(Action.ACTION_STOP);
    }

    public void accToggle(View view) {
        ToggleButton accToggle = findViewById(R.id.accToggle);
        boolean checked = accToggle.isChecked();
        accToggle.setChecked(!checked);
        if (checked) {
            requestAction(Action.ACTION_ACC_ON);
        } else {
            requestAction(Action.ACTION_ACC_OFF);
        }
    }

    public void lockDoors(View view) {
        requestAction(Action.ACTION_LOCK);
    }

    public void unlockDoors(View view) {
        requestAction(Action.ACTION_UNLOCK);
    }

    public void showLag(View view) {
        CarRemoteApplication app = getCarRemoteApplication();
        Calendar lastMessageTime = app.getLastMessageTime();
        String msg;
        if (lastMessageTime == null) {
            msg = "No messages yet";
        } else {
            long lagSeconds = (app.now().getTimeInMillis() - lastMessageTime.getTimeInMillis()) / 1000;
            msg = String.format("Lag: %s", Utils.formatDuration(lagSeconds));
        }
        app.showShortToast(msg);
    }

    private void requestAction(Action action) {
        getCarRemoteApplication().requestAction(action);
    }

    private CarRemoteApplication getCarRemoteApplication() {
        return (CarRemoteApplication) getApplication();
    }

    private BroadcastReceiver connectionStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Integer driftSec = intent.getExtras() == null ? null : (Integer) intent.getExtras().get("driftSec");
            runInLooper(new Runnable() {
                @Override
                public void run() {
                    showCurrentStatus(driftSec);
                }
            });
        }
    };

    private void showCurrentStatus(Integer driftSec) {
        CarRemoteApplication app = getCarRemoteApplication();
        Status carStatus = app.getCarStatus();
        ImageView icon = findViewById(R.id.connectionStatus);
        boolean alive = app.isConnectionAlive();
        if (carStatus != null) {
            showStatus(carStatus);
            if (alive) {
                icon.setImageResource(R.drawable.ic_cloud_ok);
            } else {
                icon.setImageResource(R.drawable.ic_time);
            }
            findViewById(R.id.autostartToggle).setEnabled(alive && !app.getSettings().isEmpty());
        } else {
            icon.setImageResource(R.drawable.ic_cloud_off);
        }
        TextView driftView = findViewById(R.id.driftView);
        if (driftSec != null) {
            driftView.setText(String.format("drift: %s", Utils.formatDuration(driftSec)));
        }
    }
}
