package me.ippolitov.car.remote;

import lombok.extern.slf4j.Slf4j;
import me.ippolitov.car.lib.Buffers;
import me.ippolitov.car.remote.message.Message;
import me.ippolitov.car.remote.message.MsgSource;
import me.ippolitov.car.remote.message.Nonce;
import org.libsodium.jni.Sodium;
import java.nio.ByteBuffer;
import java.util.EnumMap;
import java.util.Map;

import static me.ippolitov.car.remote.CryptoKey.*;

@Slf4j
public class Crypto {

    private static final Map<MsgSource, byte[]> keys = new EnumMap<>(MsgSource.class);

    static {
        byte[] convertedKey = convertKey(remoteToControllerKey);
        keys.put(MsgSource.REMOTE, convertedKey);
        keys.put(MsgSource.CAR_CONTROLLER, convertedKey);
    }

    private static byte[] convertKey(int[] intKey) {
        byte[] key = new byte[intKey.length];
        for (int i = 0; i < key.length; ++i) {
            key[i] = (byte) intKey[i];
        }
        return key;
    }

    public static ByteBuffer encrypt(Message message) {
        int plaintextLen = message.getData().limit();
        byte[] ciphertext = new byte[Sodium.crypto_secretbox_macbytes() + plaintextLen];
        Nonce nonce = message.getNonce();
        ByteBuffer nonceBuffer = nonce.pack();
        byte[] key = keys.get(nonce.getSource());
        Sodium.crypto_secretbox_easy(ciphertext, message.getData().array(), plaintextLen, nonceBuffer.array(), key);
        ByteBuffer result = Buffers.allocateBuffer().put(nonceBuffer).put(ciphertext);
        result.flip();
        return result;
    }

    public static Message decrypt(ByteBuffer nonceAndCiphertext) {
        if (nonceAndCiphertext.remaining() < Sodium.crypto_secretbox_noncebytes() + Sodium.crypto_secretbox_macbytes()) {
            return null;
        }
        Nonce nonce = Nonce.readFrom(nonceAndCiphertext);
        byte[] ciphertext = new byte[nonceAndCiphertext.remaining()];
        nonceAndCiphertext.get(ciphertext);
        byte[] plaintext = new byte[ciphertext.length - Sodium.crypto_secretbox_macbytes()];
        byte[] key = keys.get(nonce.getSource());
        if (key == null) {
            log.warn("Encryption key unknown for source {}", nonce.getSource());
            return null;
        }
        if (Sodium.crypto_secretbox_open_easy(plaintext, ciphertext, ciphertext.length, nonce.getOriginal().array(), key) != 0) {
            return null;
        }
        return new Message(plaintext, nonce);
    }
}
