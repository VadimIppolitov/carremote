package me.ippolitov.car.remote;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarSettings {
    /** unsigned 16-bit */
    int version = 0; // TODO change to -1 when ReportSettings msgs are sent
    /** signed 16-bit */
    int driftCompensation;
    /** unsigned 16-bit */
    int engineTimeoutMillis;
    boolean autostartEnabled;
    /** unsigned 16-bit */
    int autostartIntervalMinutes;
    /** unsigned 16-bit */
    int autostartDurationMinutes;

    public boolean isEmpty() {
        return version == -1;
    }
}
