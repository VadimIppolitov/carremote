package me.ippolitov.car.remote.message.types;

import lombok.Getter;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.MsgSource;
import me.ippolitov.car.remote.message.MsgType;

import java.nio.ByteBuffer;
import java.util.Calendar;

@Getter
public class RequestEvents extends CarMessage.Sendable {
    private int firstSeq; // unsigned 16-bit

    public RequestEvents(int firstSeq, Calendar sendTime) {
        this();
        this.firstSeq = firstSeq;
        this.setSecondsSinceEpoch((int) (sendTime.getTimeInMillis() / 1000));
        this.setSource(MsgSource.REMOTE);
    }

    private RequestEvents() {
        super(MsgType.MSG_REQUEST_ACTION);
    }

    @Override
    protected void doPack(ByteBuffer buffer) {
        putUnsignedShort(buffer, firstSeq);
    }
}
