package me.ippolitov.car.remote;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import lombok.extern.slf4j.Slf4j;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.types.EventList;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class NotificationController {
    private static final String GROUP_KEY_ACTIONS = "me.ippolitov.car.ACTIONS";
    private static final int SUMMARY_NOTIFICATION_ID = 1;
    private AtomicInteger notificationId = new AtomicInteger(SUMMARY_NOTIFICATION_ID);

    private Application app;

    public NotificationController(Application app) {
        this.app = app;
    }

    public void initForeground(Service udpService, Intent openIntent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(udpService, 0, openIntent, 0);
        String channelId = getNotificationChannel();
        Notification summaryNotification = new NotificationCompat.Builder(udpService, channelId)
                .setOngoing(true)
                .setGroupSummary(true)
                .setGroup(GROUP_KEY_ACTIONS)
                .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_CHILDREN)
                .setSmallIcon(R.drawable.ic_car)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentTitle("Car service")
                .setContentIntent(pendingIntent).build();
        udpService.startForeground(SUMMARY_NOTIFICATION_ID, summaryNotification);
    }

    private String getNotificationChannel() {
        // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? doCreateOngoingNotificationChannel() : "";
    }

    public NotificationManager getNotificationManager() {
        NotificationManager service = (NotificationManager)app.getSystemService(Context.NOTIFICATION_SERVICE);
        if (null == service) {
            log.error("Failed to get notification service");
        }
        return service;
    }

    public void showNotification(EventList.Event r) {
        String success = r.isSuccess() ? "OK" : "FAIL";
        String msg = String.format("%s(%s): %s", r. getEvent().name(), r.getInitiator().name(), success);
        NotificationChannels channel;
        int iconId;
        if (r.isSuccess()) {
            channel = NotificationChannels.OK;
            iconId = R.drawable.ic_done;
        } else {
            channel = NotificationChannels.FAIL;
            iconId = R.drawable.ic_not_done;
        }
        showNotification(msg, channel, iconId, r.getSecondsSinceEpoch() + CarMessage.DRIFT_HACK);
    }

    private void showNotification(String text, NotificationChannels channel, int iconId, int secondsSinceEpoch) {
        Intent notificationIntent = new Intent(app, MainRemoteActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(app, 0, notificationIntent, 0);
        createNotificationChannel(channel);

        Notification eventNotification = new NotificationCompat.Builder(app, channel.getChannelId())
                .setGroup(GROUP_KEY_ACTIONS)
                .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_CHILDREN)
                .setSmallIcon(iconId)
                .setVibrate(channel.getVibrationPattern())
                .setAutoCancel(true)
                .setContentText(text)
                .setWhen(1000L * secondsSinceEpoch)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(app);
        notificationManager.notify(notificationId.incrementAndGet(), eventNotification);
    }

    private void createNotificationChannel(NotificationChannels channel) {
        // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            doCreateNotificationChannel(channel);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void doCreateNotificationChannel(NotificationChannels channel) {
        @SuppressLint("WrongConstant") // I dunno wtf
        NotificationChannel chan = new NotificationChannel(channel.getChannelId(), channel.name() + " events", NotificationManager.IMPORTANCE_HIGH);

        chan.setLightColor(channel.getLightColor());
        chan.setVibrationPattern(channel.getVibrationPattern());
        NotificationManager notificationManager = getNotificationManager();
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(chan);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String doCreateOngoingNotificationChannel() {
        String channelId = "car_fg_notification";
        NotificationChannel chan = new NotificationChannel(channelId, "Ongoing notification", NotificationManager.IMPORTANCE_NONE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getNotificationManager().createNotificationChannel(chan);
        return channelId;
    }
}
