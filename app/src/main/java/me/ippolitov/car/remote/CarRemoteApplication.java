package me.ippolitov.car.remote;

import android.app.*;
import android.content.*;
import android.os.IBinder;
import android.widget.Toast;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import me.ippolitov.car.lib.*;
import me.ippolitov.car.remote.message.Action;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.types.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

@Slf4j
public class CarRemoteApplication extends Application {

    private static final int KEEP_ALIVE_SECONDS = 30;
    private static final int CONNECTION_TIMEOUT_SECONDS = 100;

    private AlarmReceiver alarmReceiver = new AlarmReceiver();

    @Getter
    private DataMediator dataMediator;
    @Getter
    private IncomingMessageProcessor incomingMessageProcessor = new IncomingMessageProcessor(this);

    private NotificationController notificationController = new NotificationController(this);

    @Getter
    @Setter
    private Status carStatus;

    @Getter
    @Setter
    private CarSettings settings = new CarSettings();

    @Override
    public void onCreate() {
        super.onCreate();
        log.info("Application is starting...");

        System.loadLibrary("sodiumjni");

        dataMediator = new DataMediator();
        dataMediator.addOtherEnd(incomingMessageProcessor);

        getApplicationContext().registerReceiver(alarmReceiver, new IntentFilter(AlarmReceiver.KEEP_ALIVE_INTENT));

        Intent udpIntent = new Intent(this, CarUDPService.class);
        this.startService(udpIntent);
        this.bindService(udpIntent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                log.info("UDP service connected!");
                CarUDPService udpService = ((CarUDPService.UDPBinder) binder).getService();
                dataMediator.setUdpService(udpService);
                udpService.setDataMediator(dataMediator);
                notificationController.initForeground(udpService, new Intent(udpService, MainRemoteActivity.class));

                requestAction(Action.ACTION_STATUS);

                AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(AlarmReceiver.KEEP_ALIVE_INTENT);
                PendingIntent recurringIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                final int keepAliveMillis = KEEP_ALIVE_SECONDS * 1000;
                alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, keepAliveMillis, keepAliveMillis, recurringIntent);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                dataMediator.setUdpService(null);
            }
        }, 0);
    }

    public void requestAction(Action action) {
        CarMessage.Sendable message = new RequestAction(action, now());
        sendCarMessage(message);
    }

    public void requestEvents(int firstSeq) {
        CarMessage.Sendable message = new RequestEvents(firstSeq, now());
        sendCarMessage(message);
    }

    public void applySettings() {
        settings.setVersion(inc16bit(settings.getVersion()));
        CarMessage.Sendable message = new ApplyCarSettings(this.settings, now());
        sendCarMessage(message);
    }

    private int inc16bit(int num) {
        return (num + 1) % (1 << 16);
    }

    public void sendCarMessage(CarMessage.Sendable message) {
        Packet.Builder builder = Packet.builder().putMagic();
        builder.putData(message.pack());
        dataMediator.sendViaUDP(builder.build());
    }

    public void showShortToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public Calendar getLastMessageTime() {
        return incomingMessageProcessor.getLastMessageTime();
    }

    public boolean isConnectionAlive() {
        Calendar tooOld = now();
        tooOld.add(Calendar.SECOND, -CONNECTION_TIMEOUT_SECONDS);
        Calendar lastMessageTime = getLastMessageTime();
        return lastMessageTime != null && lastMessageTime.after(tooOld);
    }

    public Calendar now() {
        return GregorianCalendar.getInstance();
    }

    public void showNotification(EventList.Event event) {
        notificationController.showNotification(event);
    }
}
