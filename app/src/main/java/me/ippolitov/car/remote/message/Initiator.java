package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum Initiator {
    UNKNOWN(0),
    BY_USER(41),
    BY_SCHEDULE(42),
    BY_ENVIRONMENT(43),
    BY_ERROR(44),
    ;

    private final byte code;

    Initiator(int code) {
        this.code = (byte) code;
    }

    public static Initiator byCode(byte code) {
        for (Initiator initiator : values()) {
            if (initiator.code == code) {
                return initiator;
            }
        }
        return UNKNOWN;
    }
}