package me.ippolitov.car.remote.message;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import me.ippolitov.car.lib.Buffers;
import org.libsodium.jni.Sodium;

import java.nio.ByteBuffer;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Nonce {

    private ByteBuffer original;
    private final int secondsSinceEpoch;
    private final int nanoTimeInt;
    private final MsgSource source;

    public static Nonce create(int secondsSinceEpoch, MsgSource source) {
        return new Nonce(null, secondsSinceEpoch, (int) System.nanoTime(), source);
    }

    public static Nonce readFrom(ByteBuffer buffer) {
        ByteBuffer nonceBuffer = allocNonceBuffer();
        buffer.get(nonceBuffer.array());
        int secondsSinceEpoch = nonceBuffer.getInt();
        int nanoTimeInt = nonceBuffer.getInt();
        MsgSource source = MsgSource.byCode(nonceBuffer.get());
        source = MsgSource.CAR_CONTROLLER;  //  TODO temporary stuff until the controller itself gets updated
        nonceBuffer.rewind();
        return new Nonce(nonceBuffer, secondsSinceEpoch, nanoTimeInt, source);
    }

    public ByteBuffer pack() {
        ByteBuffer nonce = allocRandomNonceBuffer();
        //  Overwrite some data at the start
        nonce.putInt(secondsSinceEpoch);
        nonce.putInt(nanoTimeInt);
        nonce.put(source.getCode());
        nonce.rewind();
        return nonce;
    }

    private static ByteBuffer allocNonceBuffer() {
        return Buffers.allocateBuffer(Sodium.crypto_secretbox_noncebytes());
    }

    private static ByteBuffer allocRandomNonceBuffer() {
        ByteBuffer nonce = allocNonceBuffer();
        Sodium.randombytes_buf(nonce.array(), nonce.capacity());
        return nonce;
    }
}
