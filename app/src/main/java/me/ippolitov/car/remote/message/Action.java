package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum Action {
    UNKNOWN(0),
    ACTION_START(21),
    ACTION_STOP(22),
    ACTION_STATUS(23),
    ACTION_LOCK(24),
    ACTION_UNLOCK(25),
    ACTION_ACC_ON(26),
    ACTION_ACC_OFF(27),
    ;

    private final byte code;

    Action(int code) {
        this.code = (byte) code;
    }

    public static Action byCode(int code) {
        for (Action action : values()) {
            if (action.code == code) {
                return action;
            }
        }
        return UNKNOWN;
    }
}
