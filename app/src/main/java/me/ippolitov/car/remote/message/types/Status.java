package me.ippolitov.car.remote.message.types;

import lombok.Getter;
import me.ippolitov.car.remote.message.CarMessage;
import me.ippolitov.car.remote.message.Gear;
import me.ippolitov.car.remote.message.MsgType;
import me.ippolitov.car.remote.message.StartStatus;

import java.nio.ByteBuffer;

@Getter
public class Status extends CarMessage {
    /** Is the engine currently started because of remote control request */
    private boolean ignition;
    /** Is the generator on */
    private boolean generator;
    /** What gear is the transmission set to */
    private Gear gear;
    /** What temperature does the RTC have (in deg. Celsius, signed 8-bit) */
    private byte temperature;
    /** What is the last status of a remote-requested start */
    private StartStatus lastStatus;
    /** How long did it take to remotely start the engine last time (in milliseconds, unsigned 16-bit) */
    private int lastStarterTime;
    /** Is the ACC relay on */
    private boolean acc;
    /** The sequence number of the last event known to the car controller (unsigned 16-bit) */
    private int lastEventSeq;
    /** The number of minutes before the upcoming scheduled start (unsigned 16-bit) */
    private int minutesUntilScheduledStart;

    private Status() {
        super(MsgType.MSG_STATUS);
    }

    public static Status parse(ByteBuffer buffer) {
        Status result = new Status();
        result.ignition = getBoolean(buffer);
        result.generator = getBoolean(buffer);
        result.gear = Gear.byCode((char) buffer.get());
        result.temperature = buffer.get();
        result.lastStatus = StartStatus.byCode(buffer.get());
        result.lastStarterTime = getUnsignedShort(buffer);
        result.acc = getBoolean(buffer);
        result.lastEventSeq = getUnsignedShort(buffer);
        result.minutesUntilScheduledStart = getUnsignedShort(buffer);
        return result;
    }
}
