package me.ippolitov.car.remote.message;

import lombok.Getter;

@Getter
public enum StartStatus {
    UNKNOWN(0),
    STARTED(1),
    HOLD(2),
    ENGINE_ERROR(3),
    NOT_PARKED(4),
    ALREADY_STARTED(5),
    ENGINE_TIMEOUT(6),
    STARTING(7),
    VERIFYING(8),
    ;

    private final byte code;

    StartStatus(int code) {
        this.code = (byte) code;
    }

    public static StartStatus byCode(int code) {
        for (StartStatus action : values()) {
            if (action.code == code) {
                return action;
            }
        }
        return UNKNOWN;
    }
}
