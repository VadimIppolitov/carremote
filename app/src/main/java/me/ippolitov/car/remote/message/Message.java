package me.ippolitov.car.remote.message;

import lombok.Getter;
import me.ippolitov.car.lib.Buffers;

import java.nio.ByteBuffer;

@Getter
public class Message {
    private ByteBuffer data;
    private Nonce nonce;

    public Message(ByteBuffer data, Nonce nonce) {
        this.data = data;
        this.nonce = nonce;
    }

    public Message(byte[] data, Nonce nonce) {
        this(Buffers.wrap(data), nonce);
    }
}
