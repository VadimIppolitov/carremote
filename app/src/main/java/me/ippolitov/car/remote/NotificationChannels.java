package me.ippolitov.car.remote;

import android.graphics.Color;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NotificationChannels {
    OK("car_event_ok", new long[]{10L, 600L, 300L, 800L}, Color.GREEN),
    FAIL("car_event_fail", new long[]{10L, 250L, 250L, 250L, 250L, 250L, 100L, 100L, 100L, 100L}, Color.MAGENTA);

    private String channelId;
    private long[] vibrationPattern;
    private int lightColor;
}
